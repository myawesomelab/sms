FROM maven:3.8.3-openjdk-17
WORKDIR /opt/app
EXPOSE 9090
COPY . .
RUN mvn clean install
WORKDIR /opt/app/sms-api
CMD mvn spring-boot:run