# Student Management System (SMS)
Student Management System (SMS) is a microservice containing two APIS:
* POST /students
* GET /students

The openapi yaml specs of this microservice is at sms/sms-api/src/main/resources/swagger/sms-api.yaml

### Prerequisites
In order to setup and run this microservice, you must have docker installed on your machine.

### Setup Instructions
Please execute the following steps in order to setup SMS on your machine:
* Open the terminal
* Go to the path where you have cloned the sms repository.
* Execute the following command to build the docker image: `docker build . -t sms:latest`. This would take some time, since docker base image would be pulled up, and maven dependencies would be downloaded.
* Execute the following command to pull the mongo and mongo-express images and run all the containers: `docker-compose up`. This command would take some time to complete, when it would be run the first time. This would start mongo on port 27017, mongo express on port 9091, and finally the sms microservice on port 9090. Please note that mongo-express is optional, and only have been included for troubleshooting purposes.
 
### Test Instructions
You may use curl, postman, or any other http client to test the APIs as part of this microservice. Example requests for both the APIs are given below:

# Creating a student

POST http://localhost:9090/v1/students

```
{
  "firstName": "Sofia",
  "lastName": "Iqbal",
  "department": "SE"
}
```

# Retrieving the student with optional filter

http://localhost:9090/v1/students?firstName=Sofia