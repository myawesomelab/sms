package com.daleel.sms;

import com.daleel.sms.entity.Student;
import com.daleel.sms.repository.StudentRepository;
import com.daleel.sms.specification.StudentCriteria;
import com.sms.data.dto.StudentInfo;
import com.sms.data.dto.StudentListResponse;
import com.sms.data.dto.StudentRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    private final StudentCriteria studentCriteria;

    private final MongoTemplate mongoTemplate;

    public StudentService(StudentRepository studentRepository, StudentCriteria studentCriteria,
            MongoTemplate mongoTemplate) {
        this.studentRepository = studentRepository;
        this.studentCriteria = studentCriteria;
        this.mongoTemplate = mongoTemplate;
    }

    public StudentListResponse getStudents(String firstName, String lastName, String department) {

        Query query = studentCriteria.createStudentCriteria(firstName, lastName, department);
        List<Student> students = mongoTemplate.find(query, Student.class);

        List<StudentInfo> studentInfoList = mapStudentToStudentInfo(students);

        StudentListResponse studentListResponse = new StudentListResponse();
        studentListResponse.setStudents(studentInfoList);

        return studentListResponse;
    }

    private List<StudentInfo> mapStudentToStudentInfo(List<Student> students) {
        List<StudentInfo> studentInfoList = new ArrayList<>();

        for (Student student: students) {
            StudentInfo studentInfo = new StudentInfo();
            studentInfo.setStudentId(student.getId());
            studentInfo.setFirstName(student.getFirstName());
            studentInfo.setLastName(student.getLastName());
            studentInfo.setDepartment(student.getDepartment());
            studentInfoList.add(studentInfo);
        }

        return studentInfoList;
    }

    public Student createStudent(StudentRequest request) {

        Student student = new Student();
        student.setFirstName(request.getFirstName());
        student.setLastName(request.getLastName());
        student.setDepartment(request.getDepartment());
        return studentRepository.save(student);
    }
}
