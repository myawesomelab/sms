package com.daleel.sms;

import com.sms.data.dto.StudentListResponse;
import com.sms.data.dto.StudentRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/v1/students")
    @ResponseBody
    public ResponseEntity<StudentListResponse> getStudents(@RequestParam(required = false) String firstName,
            @RequestParam(required = false) String lastName, @RequestParam(required = false) String department) {
        StudentListResponse studentListResponse = studentService.getStudents(firstName, lastName, department);

        return ResponseEntity.ok(studentListResponse);
    }

    @PostMapping("/v1/students")
    @ResponseBody
    public ResponseEntity<Void> createStudent(@RequestBody StudentRequest studentRequest) {

        studentService.createStudent(studentRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }
}
