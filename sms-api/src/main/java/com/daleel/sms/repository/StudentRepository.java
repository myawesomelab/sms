package com.daleel.sms.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.daleel.sms.entity.Student;

public interface StudentRepository extends MongoRepository<Student, String> {

}
