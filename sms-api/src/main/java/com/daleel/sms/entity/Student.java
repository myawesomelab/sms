package com.daleel.sms.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("student")
@Getter
@Setter
public class Student {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private String department;

}
