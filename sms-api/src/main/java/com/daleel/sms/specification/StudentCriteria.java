package com.daleel.sms.specification;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class StudentCriteria {

    public Query createStudentCriteria(String firstName, String lastName, String department) {

        Criteria criteria = new Criteria();

        if (firstName != null) {
            criteria = criteria.and("firstName").is(firstName);
        }
        if (lastName != null) {
            criteria = criteria.and("lastName").is(lastName);
        }
        if (department != null) {
            criteria = criteria.and("department").is(department);
        }

        Query query = new Query(criteria);
        return query;

    }

}
