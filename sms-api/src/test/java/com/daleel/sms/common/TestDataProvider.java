package com.daleel.sms.common;

import com.daleel.sms.entity.Student;
import com.sms.data.dto.StudentRequest;

public class TestDataProvider {

    public static Student getStudent() {

        Student student = new Student();
        student.setId("qwertyuiop");
        student.setFirstName("Sofia");
        student.setLastName("Iqbal");
        student.setDepartment("CS");
        return student;
    }

    public static StudentRequest getStudentRequest() {

        StudentRequest studentRequest = new StudentRequest();
        studentRequest.setFirstName("firstName");
        studentRequest.setLastName("lastName");
        studentRequest.setDepartment("test");
        return studentRequest;
    }

    private TestDataProvider() {

    }
}
