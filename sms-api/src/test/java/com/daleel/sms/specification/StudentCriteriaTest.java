package com.daleel.sms.specification;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.query.Query;

@ExtendWith(MockitoExtension.class)
public class StudentCriteriaTest {

    @InjectMocks
    private StudentCriteria studentCriteria;

    @Test
    public void createStudentCriteriaTest() {

        Query query = studentCriteria.createStudentCriteria("Sofia", "Iqbal", "CS");

        Assertions.assertNotNull(query);
        Assertions.assertEquals("Query: { \"firstName\" : \"Sofia\", \"lastName\" : \"Iqbal\", \"department\" : " +
                "\"CS\"}, Fields: {}, Sort: {}", query.toString());

    }

}
