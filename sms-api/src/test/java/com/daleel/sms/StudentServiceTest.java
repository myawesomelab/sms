package com.daleel.sms;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import com.daleel.sms.common.TestDataProvider;
import com.daleel.sms.entity.Student;
import com.daleel.sms.repository.StudentRepository;
import com.daleel.sms.specification.StudentCriteria;
import com.sms.data.dto.StudentListResponse;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {

    @Mock
    private MongoTemplate mongoTemplate;

    @Mock
    private StudentRepository studentRepository;

    @Mock
    private StudentCriteria studentCriteria;

    @InjectMocks
    private StudentService studentService;

    @Test
    void createStudentTest() {

        Mockito.when(studentRepository.save(Mockito.any(Student.class))).thenReturn(TestDataProvider.getStudent());

        Student student = studentService.createStudent(TestDataProvider.getStudentRequest());

        Assertions.assertNotNull(student);
        Assertions.assertEquals("qwertyuiop", student.getId());

    }

    @Test
    void getStudentsTest() {

        Mockito.when(studentCriteria.createStudentCriteria(Mockito.any(), Mockito.any(),
                Mockito.any())).thenReturn(new Query());

        Mockito.when(mongoTemplate.find(Mockito.any(Query.class), Mockito.any()))
                .thenReturn(Arrays.asList(TestDataProvider.getStudent()));

        StudentListResponse response = studentService.getStudents(null, null, null);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(1, response.getStudents().size());

    }

    @Test
    void getStudentsTest_withFilter() {

        Mockito.when(studentCriteria.createStudentCriteria(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString())).thenReturn(new Query());

        Mockito.when(mongoTemplate.find(Mockito.any(Query.class), Mockito.any()))
                .thenReturn(Arrays.asList(TestDataProvider.getStudent()));

        StudentListResponse response = studentService.getStudents("Sofia", "Iqbal", "CS");

        Assertions.assertNotNull(response);
        Assertions.assertEquals(1, response.getStudents().size());

    }

}
